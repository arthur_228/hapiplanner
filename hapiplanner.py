import configparser
import os
import time as base_time
from datetime import timedelta, datetime, date, time

VERSION = '1.1'

#CLASS FOR PROCESSING DAYS IN A SCHDEULE
class schedule_item(object):
	def __init__(self, index, game, time = '$'):
		self.index = index
		self.game = game
		if time == '$':
			self.time = f" Monday, November 13, 2023 {sett['STREAM_TIME']} "
		else:
			self.time = '<t:' + str(time) + ':F>'

	@classmethod
	def query(cls, index):
		game = input(f"What are you doing on {weekdays[index]}? [{sett['DAYOFF_TEXT'] or 'skip'}] ") or sett['DAYOFF_TEXT'] #game name
		if game != sett['DAYOFF_TEXT']:
			new_time = input(f"Reschedule from [{sett['STREAM_TIME']}]? ").strip() or sett['STREAM_TIME']
			time = monday_null_time + index*86400 + (int(new_time[:2])*60 +int(new_time[3:]))*60 #for time corrections
		else:
			time = ''	
		os.system('cls')
		return cls(index,game,time)

	def print(self):
		if self.game != '':
			if self.game == sett['DAYOFF_TEXT']:
				print(f"{sett['DAYOFF_BULLET']} {weekdays[self.index]} {sett['PUNCTUATION']} {self.game}")
			else:
				print(f"{sett['LIST_BULLET']} {self.time} {sett['PUNCTUATION']} {self.game}")


def print_schedule(schedule):
	print(f"{sett['HEADER']}\n")
	for item in schedule:
		item.print()
	print(f"\n\
{sett['PS_HEADER']}")

#for your ini $ needs
def inline_variable_handler(setting_name, variable, replacement, default_value = ''):
	if (sett[setting_name].replace(variable, "NULL") != sett[setting_name]):
		sett[setting_name] = sett[setting_name].replace(variable, replacement)
	else:
		sett[setting_name] = sett[setting_name] + default_value

#MAIN BODY
#initialization
#time handling
weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
weekday = (int(datetime.today().strftime('%w')))%7
monday_null_time = int(base_time.mktime(datetime.combine(datetime.today(), time(0,0,0,0)).timetuple())) - (weekday-1)*86400

#ini processing
os.system('cls')
cfg = configparser.ConfigParser(comment_prefixes=(';'))
cfg.read("settings.ini", encoding='utf-8')
profile = cfg['global_settings']['DEFAULT_PROFILE']
sett = cfg[profile]

os.system('title '+ f'hapiplanner v{VERSION}')
if cfg['global_settings']['VERSION'] > VERSION:
	input(f"WARNING: your hapiplanner.py version ({VERSION}) is older than settings file version ({cfg['global_settings']['VERSION']}), some functions might not work properly\n")
inline_variable_handler('HEADER','$WEEKDAYS',f"{datetime.fromtimestamp(monday_null_time).strftime('%b %dth')} - {datetime.fromtimestamp(monday_null_time+6*86400).strftime('%b %dth')}", f"\n*{datetime.fromtimestamp(monday_null_time).strftime('%b %dth')} - {datetime.fromtimestamp(monday_null_time+6*86400).strftime('%b %dth')}*")

#preview
os.system('cls')
sample_schedule= []
sample_schedule.append(schedule_item(0,'game'))
sample_schedule.append(schedule_item(1,sett['DAYOFF_TEXT']))

print(f"Welcome to hapiplanner!\n\
Your current profile is {profile}:\n=============================\n")
print_schedule(sample_schedule)
input(f"\n==============================\n\
Open settings.ini to edit your prefernces, restart after you save changes\n\
Otherwise press Enter to proceed\
")

#filling the schedule
os.system('cls')
real_schedule = []
for index in range(min(weekday-1,0),7):
	print(f"Editting {weekdays[index]}\n")
	tmp_day_item = schedule_item.query(index)
	real_schedule.append(tmp_day_item)

#printing schedule to be copied
print_schedule(real_schedule)
input('\nCopy your shedule above (Select & Ctrl+Shift+C)\n')