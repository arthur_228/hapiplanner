# hapiplanner

## What is it and how does it work?
It's a lil python script to assist with creating schedule. All you need is to type in games you're planning to play and you'll get a templated ready-to-post text with place for extra notes 

## Installation/Initial Setup
### 1 Python
This script is a Python code, so first you need Python on your PC if you don't have it, thus:

>1.1 Download it from official site
 [https://www.python.org/downloads/](https://www.python.org/downloads/)

>1.2 Launch installer and follow prompts: you can pick "Install Now" if you don't want to think extra time, everything should work regardless


### 2 Script itself
Now download actual script with settings file from here, for that:
> [https://gitlab.com/arthur_228/hapiplanner/-/archive/main/hapiplanner-main.zip](https://gitlab.com/arthur_228/hapiplanner/-/archive/main/hapiplanner-main.zip)

## Using the script

> 1 Run `hapiplanner.py` *(usually by double clicking it, otherwise open with Python and might as well set py files to associate with Python while you're at it)*

> 2 Answer prompts: to pick [default option] or skip day - leave it blank and hit `Enter`, until you'll get propmted to copy the results

> 3 If you want to change settings you can also change settings of the script if you open it with notepad or smth. Multiple profiles are there, change default profile to the one you want, you can add/edit profiles changing: schedule header, bulletpoints, standart stream time, day-off behaviour.


> \*Adding double streams, more formatting options can be added later if need arises
